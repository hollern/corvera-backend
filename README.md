# Introduction
Corvera is a python Flask based JSON API to support a React and React Native front-end. While this project was built with a certain end-use in mind, it can serve as a great building block for any project that uses a python backend. This project was built to support a multi-tenant SaaS approach.

## Dependencies
- flask
- flask-restful
- flask-migrate
- flask-sqlalchemy
- flask-jwt-extended
- flask-marshmallow, marshmallow, sqlalchemy-marshmallow

## Migrations
Migrations can be made which will generate the PostgreSQL schema based upon the defined database models.

## Authorization
Corvera uses JSON Web Tokens (JWTs) to authenticate users. JWTs allow for a secure token that can expire after a specified amount of time. The front-end simply POSTs an email and password and, after verification, corvera returns a JWT that can be used to identify the current user. The JWT can be stored in the client headers as a Bearer token.

## JSON Parsing
Corvera uses the popular python library Marshmallow for parsing inbound and outbound JSON. Schema's are generated based on a Flask database model. These schema's control what JSON data is serialized from the model and what data is allowed to be processed from the body of a request. Utilizing the Marshmallow schemas allow a structured way to control what data is exposed to the consumer of the API.

````python
data = model.Property.load({'id': 1, 'name': 'Test 12345'})
````

## Project Structure
The project is organized into 3 core packages:
- routes
- models
- utilities (common)

# Routes
## Dynamic Routing
The dynamic routes handles the majority of inbound requests from the client. Allowed routes are specified within the **dynamic** module. The specification for routes is a dictionary which also contains the database model that is used to query, load, and validate the data.

````python
routes = {
    'properties': {'model': model.Property},
    ...
}
````

## Special Routes
Three modules exist for special routes:
- users: used to create and query users
- authentication: used to authenticate and issue JWTs
- companies: this special route is needed as the Company model is used to separate the **tenants** of the SaaS-style database

# Models
## Schema
Within the models package a module exists for each database entity; i.e., users, companies, work requests, etc. Each module first contains a Marshmallow schema which specifies the model attributes (database columns) that can be exposed via the API. Methods can be added to the schema that can validate data. Data validation can be executed at various times using the built-in decorators. The decorator @pre_load validates right before the **load** method is called upon the schema, a similar method exists before the **dump** method is called (@pre_dump). Also, the @validates method will run a code block based on the value of a specified attribute.

## Model
After the schema class a Flask database model is declared to model the attributes of all the needed entities. These models inherit from the CoreModel (app.utilities.mixins) class which inherits from flask-sqlalchemy's standard database model class.

````python
class CoreModel(db.Model):
    __abstract__ = True
````

## Association Tables
There exist a few association table modules. They are prefixed with assn_. In these association modules a schema is not declared nor is a database model. These associations won't need to be referenced in these ways. Instead, a simple **Table** can be declared with the primary keys of both associated tables.

# Utilities (Common)
## Mixins
The mixins module contains an abstract database model (CoreModel) which is a parent class to all application models. CoreModel adds a few basic attributes like id, created, and modified which are relevant to all application models. A variety of methods are also added which are common to the application models. The methods create, save, update, and delete provide core database functionality for CRUD. The methods load, dump, and validate extend the Marshmallow schema methods.

## Filters
The filters module was built for GET requests. The first of the two main methods of this module is get_filter_parameters. This method gets a list of JSON objects and binds into a common format that can be used to design a query for filtering. The next main method is build_query. This method takes the previously specified filter parameters and dynamically builds a sqlalchemy query. Filters are received in the following format: {"field": "{}", "op": "{}", "value": "{}"}.
- field: model attribute (database column)
- op: =, <, >, <=, >=, !=, like, in
- value: the value to filter the field against

## Tokens
The tokens module contains a very small class which inherits from the Resource class of the flask_restful library. The class declared in this module is called ProtectedResource which simply applies the @jwt_required decorator to every HTTP method in a given Resource. The ProtectedResource is the parent of the dynamic routing resources which require an authenticated user to make requests to the endpoints.

## Multitenancy
This module contains a class that takes in data to be submitted to the database and the current user and ensures that the data being created belongs to the current user's company. For example: when a Unit is created it must contain a Foreign Key to a Property. This method is used to ensure that the property_id foreign key belongs to the user's company. 

**WARNING**: This module works by parsing the name of the foreign key. Ensure that ALL model foreign keys follow the same naming convention. If a model is named Model then the related Foreign Key would need to be model_id. If the model is named MyModel then the related Foreign Key would need to be named my_model_id.

The function that is used to parse the name of the foreign key can be seen below. 
````python
''.join([str_.capitalize() for str_ in entity_id.split('_')[:-1]])
````

# Improvements
## Parent class for schemas
One improvement that is a must would be to create a parent class that inherits from flask-marshmallow.Schema and is inherited by all of the model schemas. This would allow common functions to be shared and reduce duplicate code.

## Resource vs Collection
An improvement that could be made to this project would be to use a singular verb for individual resources (i.e., resources that are selected given a single unique id). This would look like property, company, user, etc. The plural endpoints should be reserved for when the API returns, or has the potential to return, a collection of items.

# Future
## Workflow Endpoints
In order to handle complex business logic, a workflows (tasks) package can be implemented. Each module within this package would contain an endpoint that accepts POST and GET methods. The POST method would be used to request the task be executed, this would return a unique id to the task. The GET method, when given a task unique id, would return the status of the task being executed in the background. Within these individual modules, additional classes and functions will be implemented to break up the task into separate components. The tasks will be executed will celery/redis.
