import requests
import datetime
import random

url = 'http://127.0.0.1:5000/api/v1'
headers = {'Content-Type': 'application/json'}

# ===== Create the first user for a newly signed-up company =====
data = {'email': 'test{}@test.com'.format(datetime.datetime.now().strftime('%Y%m%d_%H%M%S')),
        'password': 'Test#password1', 'is_first_user': True}
resp = requests.post(url + '/users/single', json=data, headers=headers)
print(resp.json())
assert resp.status_code == 201
assert 'token' in resp.text
company_id, token = resp.json()['company_id'], resp.json()['token']
headers['Authorization'] = 'Bearer {}'.format(token)

# ===== Update the company name =====
data = {'name': 'Company A{}'.format(int(round(random.random()*100, 0)))}
resp = requests.patch(url + '/companies', json=data, headers=headers)
print(resp.json())
assert resp.status_code == 201
assert 'Data created.' in resp.text

# ===== Ensure company name updated =====
resp = requests.get(url + '/companies', headers=headers)
print(resp.json())
assert resp.status_code == 200
assert resp.json()['results']['name'] == data['name']
