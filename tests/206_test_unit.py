import requests

url = 'http://127.0.0.1:5000/api/v1'
headers = {'Content-Type': 'application/json'}

# ===== Authenticate =====
body = {'email': 'test1@test.com', 'password': 'Test100!'}
resp = requests.post('http://127.0.0.1:5000/api/v1/authenticate', json=body, headers=headers)
token = resp.json()['token']
headers['Authorization'] = 'Bearer {}'.format(token)

# ===== Create a unit =====
data = {'name': 'Unit A', 'property_id': 5}
resp = requests.post(url + '/units', json=data,  headers=headers)
print(resp.json())
assert resp.status_code == 201
assert 'Data created.' in resp.text

