import requests
import datetime

url = 'http://127.0.0.1:5000/api/v1'
headers = {'Content-Type': 'application/json'}

# ===== Create user =====
data = {'email': 'test{}@test.com'.format(datetime.datetime.now().strftime('%Y%m%d_%H%M%S')),
        'password': 'Test#password1', 'is_first_user': True}
resp = requests.post(url + '/users/single', json=data, headers=headers)
print(resp.json())
assert resp.status_code == 201
assert 'token' in resp.text

# ===== Create duplicate user =====
resp = requests.post(url + '/users/single', json=data, headers=headers)
print(resp.json())
assert resp.status_code == 202
assert 'User already exists.' in resp.text

# ===== Create user without password =====
data = {'email': 'test{}@test.com'.format(datetime.datetime.now().strftime('%Y%m%d_%H%M%S')), 'is_first_user': False}
resp = requests.post(url + '/users/single', json=data, headers=headers)
print(resp.json())
assert resp.status_code == 401
assert 'password must be provided' in resp.text
