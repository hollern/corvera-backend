import requests

url = 'http://127.0.0.1:5000/api/v1'
headers = {'Content-Type': 'application/json'}

# ===== Authenticate =====
body = {'email': 'test1@test.com', 'password': 'Test100!'}
resp = requests.post('http://127.0.0.1:5000/api/v1/authenticate', json=body, headers=headers)
token = resp.json()['token']
headers['Authorization'] = 'Bearer {}'.format(token)

# ===== Create a tenant =====
data = {'first_name': 'John', 'last_name': 'Smith'}
resp = requests.post(url + '/tenants', json=data,  headers=headers)
print(resp.json())
assert resp.status_code == 201
assert 'Data created.' in resp.text

# ===== Create multiple tenants =====
data = [{'first_name': 'Person', 'last_name': 'A'}, {'first_name': 'Person', 'last_name': 'B'}]
resp = requests.post(url + '/tenants', json=data,  headers=headers)
print(resp.json())
assert resp.status_code == 201
assert 'Data created.' in resp.text
assert len(resp.json()['entities_created']) == 2

# ===== Create tenant with unknown key =====
data = {'first_name': 'John', 'last_name': 'Johnson', 'this_key_is_not_real': True}
resp = requests.post(url + '/tenants', json=data,  headers=headers)
print(resp.json())
assert resp.status_code == 401
assert 'Unknown field.' in resp.text

# ===== Create a tenant with missing first_name =====
data = {'last_name': 'Smith'}
resp = requests.post(url + '/tenants', json=data,  headers=headers)
print(resp.json())
assert resp.status_code == 401
assert 'must be provided' in resp.text

# ===== Create a tenant with missing last_name =====
data = {'first_name': 'Johnny'}
resp = requests.post(url + '/tenants', json=data,  headers=headers)
print(resp.json())
assert resp.status_code == 401
assert 'must be provided' in resp.text
