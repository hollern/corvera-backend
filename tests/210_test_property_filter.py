import requests
import json

url = 'http://127.0.0.1:5000/api/v1'
headers = {'Content-Type': 'application/json'}

# ===== Authenticate =====
body = {'email': 'test1@test.com', 'password': 'Test100!'}
resp = requests.post('http://127.0.0.1:5000/api/v1/authenticate', json=body, headers=headers)
token = resp.json()['token']
headers['Authorization'] = 'Bearer {}'.format(token)

# ===== Filter with URL params "=" =====
filters = [{'field': 'name', 'op': 'eq', 'value': 'Test Property 5000'}]
params = dict(q=json.dumps({'filters': filters}))
resp = requests.get(url + '/properties', params=params, headers=headers)
print(resp.json())
assert resp.status_code == 200

# ===== Filter with URL params "!=" =====
filters = [{'field': 'name', 'op': 'not', 'value': 'Test Property 5000'}]
params = dict(q=json.dumps({'filters': filters}))
resp = requests.get(url + '/properties', params=params, headers=headers)
print(resp.json())
assert resp.status_code == 200

# ===== Filter with URL params "<" =====
filters = [{'field': 'id', 'op': 'lt', 'value': 5}]
params = dict(q=json.dumps({'filters': filters}))
resp = requests.get(url + '/properties', params=params, headers=headers)
print(resp.json())
assert resp.status_code == 200

# ===== Filter with URL params "<=" =====
filters = [{'field': 'id', 'op': 'lte', 'value': 5}]
params = dict(q=json.dumps({'filters': filters}))
resp = requests.get(url + '/properties', params=params, headers=headers)
print(resp.json())
assert resp.status_code == 200

# ===== Filter with URL params ">" =====
filters = [{'field': 'id', 'op': 'gt', 'value': 5}]
params = dict(q=json.dumps({'filters': filters}))
resp = requests.get(url + '/properties', params=params, headers=headers)
print(resp.json())
assert resp.status_code == 200

# ===== Filter with URL params ">=" =====
filters = [{'field': 'id', 'op': 'gte', 'value': 5}]
params = dict(q=json.dumps({'filters': filters}))
resp = requests.get(url + '/properties', params=params, headers=headers)
print(resp.json())
assert resp.status_code == 200

# ===== Filter with URL params "like" =====
filters = [{'field': 'name', 'op': 'like', 'value': 'Test Property'}]
params = dict(q=json.dumps({'filters': filters}))
resp = requests.get(url + '/properties', params=params, headers=headers)
print(resp.json())
assert resp.status_code == 400
assert 'operator is not implemented' in resp.text

# ===== Filter with URL params "in" =====
filters = [{'field': 'id', 'op': 'in', 'value': ['Test Property 5000']}]
params = dict(q=json.dumps({'filters': filters}))
resp = requests.get(url + '/properties', params=params, headers=headers)
print(resp.json())
assert resp.status_code == 400
assert 'operator is not implemented' in resp.text

# ===== Filter with invalid operator =====
filters = [{'field': 'id', 'op': 'invalid', 'value': 'Test Property 5000'}]
params = dict(q=json.dumps({'filters': filters}))
resp = requests.get(url + '/properties', params=params, headers=headers)
print(resp.json())
assert resp.status_code == 400
assert 'invalid filter operator' in resp.text

# ===== Filter with field missing =====
filters = [{'op': 'equals', 'value': 'Test Property 5000'}]
params = dict(q=json.dumps({'filters': filters}))
resp = requests.get(url + '/properties', params=params, headers=headers)
print(resp.json())
assert resp.status_code == 400
assert 'field' in resp.text and 'is required for filtering' in resp.text

# ===== Filter with operator missing =====
filters = [{'field': 'id', 'value': 'Test Property 5000'}]
params = dict(q=json.dumps({'filters': filters}))
resp = requests.get(url + '/properties', params=params, headers=headers)
print(resp.json())
assert resp.status_code == 400
assert 'op' in resp.text and 'is required for filtering' in resp.text

# ===== Filter with value missing =====
filters = [{'field': 'id', 'op': 'equals'}]
params = dict(q=json.dumps({'filters': filters}))
resp = requests.get(url + '/properties', params=params, headers=headers)
print(resp.json())
assert resp.status_code == 400
assert 'value' in resp.text and 'is required for filtering' in resp.text
