import requests
import datetime
import random

url = 'http://127.0.0.1:5000/api/v1'
headers = {'Content-Type': 'application/json'}

# ===== Create the first user for a newly signed-up company =====
data = {'email': 'test{}@test.com'.format(datetime.datetime.now().strftime('%Y%m%d_%H%M%S')),
        'password': 'Test#password1', 'is_first_user': True}
resp = requests.post(url + '/users/single', json=data, headers=headers)
print(resp.json())
assert resp.status_code == 201
assert 'token' in resp.text
company_id, token = resp.json()['company_id'], resp.json()['token']
headers['Authorization'] = 'Bearer {}'.format(token)

# ===== Update the company name =====
data = {'name': 'Company A{}'.format(int(round(random.random()*100, 0)))}
resp = requests.patch(url + '/companies', json=data, headers=headers)
print(resp.json())
assert resp.status_code == 200
assert 'Data created.' in resp.text

# ===== Create first property =====
data = {'name': 'Test Property A', 'property_type': 'Apartment'}
resp = requests.post(url + '/properties', json=data,  headers=headers)
print(resp.json())
assert resp.status_code == 201
assert 'Data created.' in resp.text

property_id = resp.json()['entities_created'][0]

# ===== Create a unit =====
data = {'name': 'Unit A', 'property_id': property_id}
resp = requests.post(url + '/units', json=data,  headers=headers)
print(resp.json())
assert resp.status_code == 201
assert 'Data created.' in resp.text

unit_id = resp.json()['entities_created'][0]

# ===== Log into different company =====
url = 'http://127.0.0.1:5000/api/v1'
headers = {'Content-Type': 'application/json'}
body = {'email': 'test1@test.com', 'password': 'Test100!'}
resp = requests.post('http://127.0.0.1:5000/api/v1/authenticate', json=body, headers=headers)
token = resp.json()['token']
headers['Authorization'] = 'Bearer {}'.format(token)

# ===== Create a property =====
data = {'name': 'Test Property A', 'property_type': 'Apartment'}
resp = requests.post(url + '/properties', json=data,  headers=headers)
print(resp.json())
assert resp.status_code == 201
assert 'Data created.' in resp.text

new_property_id = resp.json()['entities_created'][0]

# ===== Create a unit =====
data = {'name': 'Unit A', 'property_id': property_id}
resp = requests.post(url + '/units', json=data,  headers=headers)
print(resp.json())
assert resp.status_code == 201
assert 'Data created.' in resp.text

new_unit_id = resp.json()['entities_created'][0]

# ===== Update unit on inaccessible property =====
data = {'name': 'Unit AAA12345', 'property_id': property_id}
resp = requests.patch(url + '/units/{}'.format(new_unit_id), json=data,  headers=headers)
print(resp.json())
assert resp.status_code == 403
assert 'Not authorized to access this resource.' in resp.text

# ===== Try to update property "id" =====
data = {'id': 99, 'name': 'tester000', 'property_type': 'Apartment'}
resp = requests.patch(url + '/properties/{}'.format(new_property_id), json=data,  headers=headers)
print(resp.json())
assert resp.status_code == 400
assert 'Unknown field.' in resp.text
