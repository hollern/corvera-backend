import requests

url = 'http://127.0.0.1:5000/api/v1'
headers = {'Content-Type': 'application/json'}

# ===== Authenticate =====
body = {'email': 'test20200226_161024@test.com', 'password': 'Test#password1'}
resp = requests.post('http://127.0.0.1:5000/api/v1/authenticate', json=body, headers=headers)
token = resp.json()['token']
headers['Authorization'] = 'Bearer {}'.format(token)

# ===== Create a property =====
data = {'name': 'Test Property 5000', 'street_addr_line_1': '1234 Test Ave',
        'city': 'Columbus', 'state': 'OH', 'property_type': 'Apartment'}
resp = requests.post(url + '/properties', json=data,  headers=headers)
print(resp.json())
assert resp.status_code == 201
assert 'Data created.' in resp.text

# ===== Create multiple properties =====
data = [{'name': 'Test Property AAA', 'property_type': 'Townhome'}, {'name': 'Test Property AAB', 'property_type': 'Townhome'}]
resp = requests.post(url + '/properties', json=data,  headers=headers)
print(resp.json())
assert resp.status_code == 201
assert 'Data created.' in resp.text
assert len(resp.json()['entities_created']) == 2

# ===== Create property with unknown key =====
data = {'name': 'Test Property AAA', 'property_type': 'Townhome', 'this_key_is_not_real': True}
resp = requests.post(url + '/properties', json=data,  headers=headers)
print(resp.json())
assert resp.status_code == 400
assert 'Unknown field.' in resp.text

# ===== Create a property with missing name =====
data = {'street_addr_line_1': '1234 Test Ave',
        'city': 'Columbus', 'state': 'OH', 'property_type': 'Apartment'}
resp = requests.post(url + '/properties', json=data,  headers=headers)
print(resp.json())
assert resp.status_code == 400
assert 'Required field is missing.' in resp.text
