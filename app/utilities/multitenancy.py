from app import models


class ForeignKeyValidator:
    @staticmethod
    def validate_company_authorized(data, current_user_company_id):
        def iter_entity_ids(ids):
            for entity_id in ids:
                model_name = ''.join([str_.capitalize() for str_ in entity_id.split('_')[:-1]])
                model = getattr(models, model_name)
                entity = model.query.filter_by(id=ids[entity_id]).first()
                if entity.company_id != current_user_company_id:
                    return False
            return True

        if type(data) is dict:
            entity_ids = {k: v for k, v in data.items() if '_id' in k}
            return iter_entity_ids(entity_ids)
        elif type(data) is list:
            for item in data:
                entity_ids = {k: v for k, v in item.items() if '_id' in k}
                has_auth = iter_entity_ids(entity_ids)
                if not has_auth:
                    return False
            return True
        else:
            return False
