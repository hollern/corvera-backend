from flask_jwt_extended import jwt_required
from flask_restful import Resource


class ProtectedResource(Resource):
    method_decorators = [jwt_required]
