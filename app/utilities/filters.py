from flask import request
import json


class Filters:
    OPERATORS = {
        'eq',
        'lt',
        'gt',
        'lte',
        'gte',
        'not',
        'in',
        'like'
    }

    @staticmethod
    def get_filter_parameters():
        try:
            if len(request.args) > 0:
                url_args = request.args.to_dict(flat=True)
                filter_params = json.loads(url_args['q'])
                return Filters.validate(filter_params)
            return {'filters': []}
        except KeyError:
            raise Filters.FilterError('Unable to parse filter parameters.')  # return {'error': 'Unable to parse filter parameters.'}

    @staticmethod
    def validate(params: dict):
        for param in params['filters']:
            # Validate each filter contains only the three desired parameters
            if 'field' not in param:
                raise Filters.FilterError('"field" is required for filtering.')  # return {'error': '"field" is required for filtering.'}
            if 'op' not in param:
                raise Filters.FilterError('"op" is required for filtering.')  # return {'error': '"op" is required for filtering.'}
            if 'value' not in param:
                raise Filters.FilterError('"value" is required for filtering.')  # return {'error': '"value" is required for filtering.'}
            if len(param) != 3:
                raise Filters.FilterError('Invalid parameter(s) for filtering.')  # return {'error': 'Invalid parameter(s) for filtering.'}
            # Validate filter operators
            if param['op'] not in Filters.OPERATORS:
                raise Filters.FilterError('"{}" is an invalid filter operator.'.format(param['op']))  # return {'error': '"{}" is an invalid filter operator.'.format(param['op'])}
        return params

    @staticmethod
    def build_query(filters, model, user):
        try:
            query = model.query.filter_by(company_id=user.company_id)
            for filtr in filters['filters']:
                if filtr['op'] == 'eq':
                    query = query.filter(getattr(model, filtr['field']) == filtr['value'])
                elif filtr['op'] == 'lt':
                    query = query.filter(getattr(model, filtr['field']) < filtr['value'])
                elif filtr['op'] == 'gt':
                    query = query.filter(getattr(model, filtr['field']) > filtr['value'])
                elif filtr['op'] == 'lte':
                    query = query.filter(getattr(model, filtr['field']) <= filtr['value'])
                elif filtr['op'] == 'gte':
                    query = query.filter(getattr(model, filtr['field']) >= filtr['value'])
                elif filtr['op'] == 'not':
                    query = query.filter(getattr(model, filtr['field']) != filtr['value'])
                elif filtr['op'] == 'in':
                    # query = query.filter(getattr(model, filtr['field']).in_(filtr['value']))
                    raise Filters.FilterError('Filters "in" operator is not implemented.')
                elif filtr['op'] == 'like':
                    # query = query.filter(getattr(model, filtr['field']).like(filtr['value']))
                    raise Filters.FilterError('Filters "like" operator is not implemented.')
                else:
                    raise Filters.FilterError('{} is an invalid operator for filtering.'.format(filtr['op']))
            return query.all()
        except AttributeError as e:
            raise Filters.FilterError(str(e))

    class FilterError(Exception):
        def __init__(self, message):
            self.message = message

        def __str__(self):
            return self.message
