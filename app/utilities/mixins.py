from marshmallow import ValidationError
from datetime import datetime
from app import db


class CoreModel(db.Model):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True, index=True, autoincrement=True)
    created = db.Column(db.DateTime, default=datetime.utcnow)
    modified = db.Column(db.DateTime, default=datetime.utcnow)

    @classmethod
    def create(cls, data, commit=True):
        if type(data) is dict:
            obj = cls(**data)
            db.session.add(obj)
            obj_list = [obj]
        elif type(data) is list:
            obj_list = []
            for item in data:
                obj = cls(**item)
                db.session.add(obj)
                obj_list.append(obj)
        else:
            raise TypeError('Data type must be dict or list when using "create" method.')
        if commit:
            db.session.commit()
        return [obj.id for obj in obj_list]

    def save(self, commit=True):
        db.session.add(self)
        if commit:
            db.session.commit()
        return self

    def update(self, update_data: dict, commit=True, **kwargs):
        for attr, value in update_data.items():
            setattr(self, attr, value)
        if commit:
            self.save()
        return self

    def remove(self, commit=True):
        db.session.delete(self)
        if commit:
            db.session.commit()

    @classmethod
    def load(cls, data, schema):
        if type(data) is dict:
            return schema[0].load(data)
        elif type(data) is list:
            return schema[1].load(data)
        else:
            raise ValidationError('Data type must be dict or list when using "load" method.')

    @classmethod
    def dump(cls, data, schema):
        if type(data) is list:
            return schema[1].dump([item.to_dict() for item in data])
        else:
            return schema[0].dump(data.to_dict())

    @classmethod
    def validate(cls, data, schema):
        if type(data) is dict:
            return schema[0].validate(data)
        elif type(data) is list:
            return schema[1].validate(data)
        else:
            raise ValidationError('Data type must be dict or list when using "validate" method.')

    def to_dict(self):
        return self.__dict__
