from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api
from flask_marshmallow import Marshmallow
from app.config import DevConfig
from flask_jwt_extended import JWTManager

app = Flask(__name__)
app.config.from_object(DevConfig)

api = Api(app)

db = SQLAlchemy(app)
migrate = Migrate(app, db)
marsh = Marshmallow(app)

jwt = JWTManager(app)

from app import routes, models
