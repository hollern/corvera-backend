from flask import request, jsonify, make_response
from flask_restful import Resource
from app.utilities import ProtectedResource
from app import app, api, db
from app import models
from flask_jwt_extended import create_access_token
import datetime
from marshmallow import ValidationError

base_route = app.config['API_ROUTE']


class User(Resource):
    def post(self, *args, **kwargs):
        try:
            # Error if request is not JSON
            if not request.is_json:
                return make_response(jsonify({'error': 'Missing JSON in request.'}), 400)

            # Get POST data
            body = request.get_json()

            # Ensure email and password are in body
            data = models.User.load(body)

            # Check if user exists
            user = models.User().query.filter_by(email=data['email']).first()
            # Create if user doesn't exist
            if user is None:
                # Create company for user to be a part of if is_first_user is True, this is to support multi-tenancy
                is_first_user = data['is_first_user']
                if is_first_user is True:
                    company = models.Company()
                    company.save()
                    data['company_id'] = company.id

                user = models.User(**{k: data[k] for k in data if k != 'password'})  # unpack parameters without password
                user.set_password(data['password'])
                user.save()

                # Issue access token
                expires = datetime.timedelta(hours=8)
                access_token = create_access_token(identity=user.public_id, expires_delta=expires)

                # If is_first_user is True then return access token and the newly create company_id
                if is_first_user is True:
                    return make_response(jsonify({'token': access_token, 'company_id': user.company_id}), 201)
                else:
                    return make_response(jsonify({'message': 'User created successfully.'}), 201)
            else:
                return make_response(jsonify({'error': 'User already exists.'}), 202)
        except ValidationError as e:
            return make_response(jsonify({'error': e.messages}), 401)
        except Exception as e:
            return make_response(jsonify({'error': 'Unknown server error. Could not authenticate.'}), 500)


class UserList(ProtectedResource):
    pass


api.add_resource(User, '{}/users/single'.format(base_route))
api.add_resource(UserList, '{}/users/list'.format(base_route))
