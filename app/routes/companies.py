from app.utilities import InvalidRouteError, ProtectedResource, Filters
from flask import request, jsonify, make_response
from flask_jwt_extended import get_jwt_identity
from app import db, app, api, models

base_route = app.config['API_ROUTE']


class Company(ProtectedResource):
    def get(self, *args, **kwargs):
        try:
            # Get current user from JWT
            current_user = get_jwt_identity()
            user = models.User.query.filter(models.User.public_id == current_user).first()
            # Query entity (ensure the id of the entity has the same company_id as the user)
            entity = models.Company.query\
                .filter_by(id=user.company_id)\
                .first()
            # Dump data from schema
            data = models.Company.dump(entity)
            # Return results
            return make_response(jsonify({'results': data}), 200)
        except InvalidRouteError as e:
            return make_response(jsonify({'error': str(e)}), 404)
        except Exception as e:
            return make_response(jsonify({'error': 'Unknown server error.'}), 500)

    def patch(self, *args, **kwargs):
        try:
            # Error if request is not JSON
            if not request.is_json:
                return make_response(jsonify({'error': 'Missing JSON in request.'}), 400)
            # Get body
            body = request.get_json()
            # Get current user from JWT
            current_user = get_jwt_identity()
            user = models.User.query.filter(models.User.public_id == current_user).first()
            # Get entity
            entity = models.Company.query.filter_by(id=user.company_id).first()

            # Ensure entity exists
            if entity is None:
                return make_response(jsonify({'error': 'Company does not exist.'}), 404)
            # Ensure user's company_id matches this entities company_id
            if user.company_id != entity.id:
                return make_response(jsonify({'error': 'User not authorized to update this company.'}), 403)

            # Load data based on schema
            data = models.Company.load(body)
            entity.update(data)
            return make_response(jsonify({'message': 'Data created.'}), 200)
        except InvalidRouteError as e:
            return make_response(jsonify({'error': str(e)}), 404)
        except Exception as e:
            return make_response(jsonify({'error': 'Unknown server error.'}), 500)


api.add_resource(Company, '{}/companies'.format(base_route))
