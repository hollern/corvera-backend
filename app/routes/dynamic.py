from app.utilities import InvalidRouteError, ProtectedResource, Filters
from flask import request, jsonify, make_response
from flask_jwt_extended import get_jwt_identity
from marshmallow import ValidationError
from app import app, api, models
from app.utilities import ForeignKeyValidator
from sqlalchemy.exc import IntegrityError

base_route = app.config['API_ROUTE']


def router(entity_type: str):
    routes = {
        'properties': {'model': models.Property},
        'tenants': {'model': models.Tenant},
        'leases': {'model': models.Lease},
        'units': {'model': models.Unit}
    }

    if entity_type not in routes:
        raise InvalidRouteError('{} is an invalid route.'.format(entity_type))

    return routes[entity_type]['model']


class Entity(ProtectedResource):
    def get(self, *args, **kwargs):
        try:
            # Get model
            model = router(kwargs['entity_type'])
            # Get current user from JWT
            current_user = get_jwt_identity()
            user = models.User.query.filter(models.User.public_id == current_user).first()
            # Query entity (ensure the id of the entity has the same company_id as the user)
            entity = model.query\
                .filter_by(company_id=user.company_id)\
                .filter_by(id=kwargs['resource_id'])\
                .all()
            # Dump data from schema
            data = model.dump(entity)
            # Return results
            return make_response(jsonify({'results': data}), 200)
        except InvalidRouteError as e:
            return make_response(jsonify({'error': str(e)}), 404)
        except ValidationError as e:
            return make_response(jsonify({'error': e.messages}), 400)
        except Exception as e:
            return make_response(jsonify({'error': 'Unknown server error.'}), 500)

    def patch(self, *args, **kwargs):
        try:
            # Get model
            model = router(kwargs['entity_type'])
            # Error if request is not JSON
            if not request.is_json:
                return make_response(jsonify({'error': 'Missing JSON in request.'}), 400)
            # Get body
            body = request.get_json()
            # Get current user from JWT
            current_user = get_jwt_identity()
            user = models.User.query.filter(models.User.public_id == current_user).first()
            # Get entity
            entity = model.query.filter_by(id=kwargs['resource_id']).first()

            # Ensure entity exists
            if entity is None:
                error_message = 'Entity with id "{}" does not exist.'.format(kwargs['resource_id'])
                return make_response(jsonify({'error': error_message}), 404)
            # Ensure user's company_id matches this entities company_id
            if user.company_id != entity.company_id:
                return make_response(jsonify({'error': 'User not authorized to update entities for this company.'}), 403)

            # Load data based on schema
            data = model.load(body)

            # Prevent authenticated user from submitting data to incorrect company
            company_authorized = ForeignKeyValidator.validate_company_authorized(data, user.company_id)
            if not company_authorized:
                return make_response(jsonify({'error': 'Not authorized to access this resource.'}), 403)

            entity.update(data)
            return make_response(jsonify({'message': 'Data created.'}), 200)
        except InvalidRouteError as e:
            return make_response(jsonify({'error': str(e)}), 404)
        except ValidationError as e:
            return make_response(jsonify({'error': e.messages}), 400)
        except Exception as e:
            return make_response(jsonify({'error': 'Unknown server error.'}), 500)

    def delete(self, *args, **kwargs):
        try:
            # Get model
            model = router(kwargs['entity_type'])
            # Get current user from JWT
            current_user = get_jwt_identity()
            user = models.User.query.filter(models.User.public_id == current_user).first()
            # Get entity
            entity = model.query.filter_by(id=kwargs['resource_id']).first()

            # Ensure entity exists
            if entity is None:
                error_message = 'Entity with id "{}" does not exist.'.format(kwargs['resource_id'])
                return make_response(jsonify({'error': error_message}), 404)
            # Ensure user's company_id matches this entities company_id
            if user.company_id != entity.company_id:
                return make_response(jsonify({'error': 'User not authorized to update entities for this company.'}), 403)

            entity.remove()
            return make_response(jsonify({'error': 'Entity deleted.'}), 204)
        except InvalidRouteError as e:
            return make_response(jsonify({'error': str(e)}), 404)
        except ValidationError as e:
            return make_response(jsonify({'error': e.messages}), 400)
        except Exception as e:
            return make_response(jsonify({'error': 'Unknown server error.'}), 500)


class EntityList(ProtectedResource):
    def get(self, *args, **kwargs):
        try:
            # Get model
            model = router(kwargs['entity_type'])
            # Get filter parameters from request
            filter_params = Filters.get_filter_parameters()
            # Get current user from JWT
            current_user = get_jwt_identity()
            user = models.User.query.filter(models.User.public_id == current_user).first()
            # Build dynamic query
            entities = Filters.build_query(filters=filter_params, model=model, user=user)
            # Dump data from schema
            data = model.dump(entities)
            # Return results
            return make_response(jsonify({'results': data}), 200)
        except InvalidRouteError as e:
            return make_response(jsonify({'error': str(e)}), 404)
        except Filters.FilterError as e:
            return make_response(jsonify({'error': str(e)}), 400)
        except ValidationError as e:
            return make_response(jsonify({'error': e.messages}), 500)
        except Exception as e:
            return make_response(jsonify({'error': 'Unknown server error.'}), 500)

    def post(self, *args, **kwargs):
        try:
            # Get model
            model = router(kwargs['entity_type'])
            # Error if request is not JSON
            if not request.is_json:
                return make_response(jsonify({'error': 'Missing JSON in request.'}), 400)
            # Get body
            body = request.get_json()
            # Get current user from JWT
            current_user = get_jwt_identity()
            user = models.User.query.filter(models.User.public_id == current_user).first()

            data = model.load(body)

            # Prevent authenticated user from submitting data to incorrect company
            company_authorized = ForeignKeyValidator.validate_company_authorized(data, user.company_id)
            if not company_authorized:
                return make_response(jsonify({'error': 'Not authorized to access this resource.'}), 403)

            # Set company_id equal to current_user's company_id
            if type(data) is dict:
                data['company_id'] = user.company_id
            elif type(data) is list:
                for item in data:
                    item['company_id'] = user.company_id
            else:
                return make_response(jsonify({'error': 'Invalid object type.'}), 400)

            entity_ids = model.create(data)
            return make_response(jsonify({'message': 'Data created.', 'entities_created': entity_ids}), 201)
        except InvalidRouteError as e:
            return make_response(jsonify({'error': str(e)}), 404)
        except ValidationError as e:
            return make_response(jsonify({'error': e.messages}), 400)
        except IntegrityError as e:
            return make_response(jsonify({'error': 'Required field is missing.'}), 400)
        except Exception as e:
            print(e)
            return make_response(jsonify({'error': 'Unknown server error.'}), 500)


api.add_resource(Entity, '{}/<entity_type>/<resource_id>'.format(base_route))
api.add_resource(EntityList, '{}/<entity_type>'.format(base_route))
