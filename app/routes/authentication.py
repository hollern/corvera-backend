from app import app, api, db
from app import models
from flask_restful import Resource
from flask import request, jsonify, make_response
from flask_jwt_extended import create_access_token
import datetime

base_route = app.config['API_ROUTE']


class Authenticate(Resource):
    def post(self):
        try:
            # Error if request is not JSON
            if not request.is_json:
                return make_response(jsonify({'error': 'Missing JSON in request.'}), 400)

            # Get email and password from body
            body = request.get_json()

            # Error if email and/or password is missing
            if 'email' not in body or 'password' not in body:
                return make_response(jsonify({'error': 'Missing email and/or password.'}), 400)

            # Validate user exists
            user = models.User().query.filter_by(email=body['email']).first()
            if user is None:
                return make_response(jsonify({'error': 'No user exists with that email address.'}), 401)
            # Validate correct password
            authorized = user.check_password(body['password'])
            if not authorized:
                return make_response(jsonify({'error': 'Invalid password for user.'}), 401)

            # Issue authentication token
            expires = datetime.timedelta(hours=8)
            access_token = create_access_token(identity=user.public_id, expires_delta=expires)

            return make_response(jsonify({'token': access_token}), 201)
        except Exception as e:
            return make_response(jsonify({'error': 'Unknown server error. Could not authenticate.'}), 500)


api.add_resource(Authenticate, '{}/authenticate'.format(base_route))
