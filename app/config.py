import os

basedir = os.path.abspath(os.path.dirname(__file__))


class DevConfig(object):
    # Core
    API_ROUTE = '/api/v1'

    # Debug
    DEBUG = True

    # SQLAlchemy
    SQLALCHEMY_DATABASE_URI = 'postgres://rcesinvu:eKzM_rXRDvkzYRKdo4FHq8th1cJDo0Kp@rajje.db.elephantsql.com:5432/rcesinvu'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # JWT
    SECRET_KEY = 'secret-key'
    JWT_SECRET_KEY = 'jwt-secret-key'
    JWT_ERROR_MESSAGE_KEY = 'error'


"""class ProdConfig(object):
    # Core
    API_ROUTE = '/api/v1'

    # Debug
    DEBUG = False

    # SQLAlchemy
    SQLALCHEMY_DATABASE_URI = os.environ['CORVERA_DB']
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # JWT
    SECRET_KEY = os.environ['CORVERA_SECRET_KEY']
    JWT_SECRET_KEY = os.environ['CORVERA_JWT_SECRET_KEY']
    JWT_ERROR_MESSAGE_KEY = 'error'"""
