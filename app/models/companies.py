from app.utilities import CoreModel
from app import db, marsh


class CompanySchema(marsh.Schema):
    class Meta:
        fields = ('name', 'twilio_sms_number')
        dump_only = ('id', 'twilio_sms_number')
        unknown = 'raise'


schemas = CompanySchema(), CompanySchema(many=True)


class Company(CoreModel):
    __tablename__ = 'companies'
    name = db.Column(db.String(299))
    twilio_sms_number = db.Column(db.String(19))

    # Relationships
    users = db.relationship('User', backref='company', lazy='dynamic')
    tenants = db.relationship('Tenant', backref='company', lazy='dynamic')
    documents = db.relationship('Document', backref='company', lazy='dynamic')
    notes = db.relationship('Note', backref='company', lazy='dynamic')
    communications = db.relationship('Communication', backref='company', lazy='dynamic')
    payments_due = db.relationship('PaymentDue', backref='company', lazy='dynamic')
    payments_made = db.relationship('PaymentMade', backref='company', lazy='dynamic')
    leases = db.relationship('Lease', backref='company', lazy='dynamic')
    units = db.relationship('Unit', backref='company', lazy='dynamic')
    work_requests = db.relationship('WorkRequest', backref='company', lazy='dynamic')

    @classmethod
    def load(cls, data, **kwargs):
        return super().load(data=data, schema=schemas)

    @classmethod
    def dump(cls, data, **kwargs):
        return super().dump(data=data, schema=schemas)

    @classmethod
    def validate(cls, data, **kwargs):
        return super().validate(data=data, schema=schemas)
