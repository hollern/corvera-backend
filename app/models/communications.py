from app.utilities import CoreModel
from app.models import communications_with_tenants
from datetime import datetime
from app import db, marsh


class CommunicationSchema(marsh.Schema):
    class Meta:
        fields = ('id', 'name', 'addr')
        dump_only = ('id')


schemas = CommunicationSchema(), CommunicationSchema(many=True)


class Communication(CoreModel):
    __tablename__ = 'communications'
    sent_time = db.Column(db.DateTime, default=datetime.utcnow)
    communication_type = db.Column(db.String(29))  # email, sms, announcement
    company_id = db.Column(db.Integer, db.ForeignKey('companies.id'), index=True, nullable=False)

    # Relationships
    tenants = db.relationship('Tenant', secondary=communications_with_tenants,
                              backref=db.backref('communication', lazy='dynamic'), lazy='dynamic')

    @classmethod
    def load(cls, data, **kwargs):
        return super().load(data=data, schema=schemas)

    @classmethod
    def dump(cls, data, **kwargs):
        return super().dump(data=data, schema=schemas)

    @classmethod
    def validate(cls, data, **kwargs):
        return super().validate(data=data, schema=schemas)
