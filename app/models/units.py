from marshmallow import pre_load, validates, ValidationError
from app import app, db, marsh
from app.utilities import CoreModel


class UnitSchema(marsh.Schema):
    class Meta:
        fields = ('id', 'name', 'property_id')
        dump_only = ('id')
        unknown = 'raise'


schemas = UnitSchema(), UnitSchema(many=True)


class Unit(CoreModel):
    __tablename__ = 'units'
    name = db.Column(db.String(99), nullable=False)
    property_id = db.Column(db.ForeignKey('properties.id'), index=True, nullable=False)
    company_id = db.Column(db.ForeignKey('companies.id'), index=True, nullable=False)

    # Relationships
    work_requests = db.relationship('WorkRequest', backref='unit', lazy='dynamic')
    documents = db.relationship('Document', backref='unit', lazy='dynamic')
    leases = db.relationship('Lease', backref='unit', lazy='dynamic')
    notes = db.relationship('Note', backref='unit', lazy='dynamic')

    @classmethod
    def load(cls, data, **kwargs):
        return super().load(data=data, schema=schemas)

    @classmethod
    def dump(cls, data, **kwargs):
        return super().dump(data=data, schema=schemas)

    @classmethod
    def validate(cls, data, **kwargs):
        return super().validate(data=data, schema=schemas)
