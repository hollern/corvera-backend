from app.utilities import CoreModel
from app.models import tenants_on_leases, communications_with_tenants
from marshmallow import pre_load, ValidationError
from app import db, marsh


class TenantSchema(marsh.Schema):
    class Meta:
        fields = ('id', 'first_name', 'last_name', 'phone_number', 'email', 'date_of_birth', 'emr_contact_name',
                  'emr_contact_relationship', 'emr_contact_phone', 'emr_contact_email', 'street_addr_line_1',
                  'street_addr_line_2', 'street_addr_line_3', 'city', 'state', 'zip', 'country', 'is_cosigner')
        dump_only = ('id')
        unknown = 'raise'


schemas = TenantSchema(), TenantSchema(many=True)


class Tenant(CoreModel):
    __tablename__ = 'tenants'
    first_name = db.Column(db.String(99), nullable=False)
    last_name = db.Column(db.String(99), nullable=False)
    phone_number = db.Column(db.String(19))
    email = db.Column(db.String(199))
    date_of_birth = db.Column(db.Date)
    taxpayer_id = db.Column(db.String(29))
    emr_contact_name = db.Column(db.String(99))
    emr_contact_relationship = db.Column(db.String(29))
    emr_contact_phone = db.Column(db.String(19))
    emr_contact_email = db.Column(db.String(199))
    street_addr_line_1 = db.Column(db.String(149))
    street_addr_line_2 = db.Column(db.String(149))
    street_addr_line_3 = db.Column(db.String(149))
    city = db.Column(db.String(149))
    state = db.Column(db.String(9))
    zip = db.Column(db.String(9))
    country = db.Column(db.String(99))
    is_cosigner = db.Column(db.Boolean, default=False)
    company_id = db.Column(db.Integer, db.ForeignKey('companies.id'), index=True, nullable=False)

    # Relationships
    work_requests = db.relationship('WorkRequest', backref='tenant', lazy='dynamic')
    leases = db.relationship('Lease', secondary=tenants_on_leases,
                             backref=db.backref('tenant', lazy='dynamic'), lazy='dynamic')
    communications = db.relationship('Communication', secondary=communications_with_tenants,
                                     backref=db.backref('tenant', lazy='dynamic'), lazy='dynamic')

    @classmethod
    def load(cls, data, **kwargs):
        return super().load(data=data, schema=schemas)

    @classmethod
    def dump(cls, data, **kwargs):
        return super().dump(data=data, schema=schemas)

    @classmethod
    def validate(cls, data, **kwargs):
        return super().validate(data=data, schema=schemas)
