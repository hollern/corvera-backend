from app.utilities import CoreModel
from app import db, marsh


class WorkRequestSchema(marsh.Schema):
    class Meta:
        fields = ('id', 'name', 'addr')
        dump_only = ('id')


schemas = WorkRequestSchema(), WorkRequestSchema(many=True)


class WorkRequest(CoreModel):
    __tablename__ = 'work_requests'
    subject = db.Column(db.String(99))
    is_entry_allowed = db.Column(db.Boolean)
    status = db.Column(db.String(29))
    priority = db.Column(db.String(29))
    date_completed = db.Column(db.Date)
    date_opened = db.Column(db.Date)
    tenant_id = db.Column(db.Integer, db.ForeignKey('tenants.id'), index=True, nullable=False)
    unit_id = db.Column(db.Integer, db.ForeignKey('units.id'), index=True, nullable=False)
    company_id = db.Column(db.ForeignKey('companies.id'), index=True, nullable=False)

    # Relationships
    documents = db.relationship('Document', backref='work_request', lazy='dynamic')

    @classmethod
    def load(cls, data, **kwargs):
        return super().load(data=data, schema=schemas)

    @classmethod
    def dump(cls, data, **kwargs):
        return super().dump(data=data, schema=schemas)

    @classmethod
    def validate(cls, data, **kwargs):
        return super().validate(data=data, schema=schemas)
