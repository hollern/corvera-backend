from werkzeug.security import generate_password_hash, check_password_hash
from marshmallow import pre_load, ValidationError
from app.utilities import CoreModel
from app import db, marsh
from uuid import uuid4


class UserSchema(marsh.Schema):
    required_fields = ['email', 'password', 'is_first_user']

    class Meta:
        fields = ('public_id', 'email', 'password', 'is_first_user', 'is_admin', 'company_id')
        load_only = ('password')  # fields to accept ONLY when loading
        dump_only = ('id', 'public_id')               # fields to accept ONLY when dumping
        unknown = 'raise'

    @pre_load
    def verify_required_fields(self, data, **kwargs):
        for field in UserSchema.required_fields:
            if field not in data:
                raise ValidationError('{} must be provided'.format(field))
        return data


schemas = UserSchema(), UserSchema(many=True)


class User(CoreModel):
    __tablename__ = 'users'
    public_id = db.Column(db.String(50), unique=True, default=uuid4)
    email = db.Column(db.String(299), index=True, unique=True, nullable=False)
    password = db.Column(db.String(128), nullable=False)
    is_admin = db.Column(db.Boolean, default=False)
    is_first_user = db.Column(db.Boolean, default=False, nullable=False)
    company_id = db.Column(db.Integer, db.ForeignKey('companies.id'), index=True, nullable=True)

    # Relationships
    documents = db.relationship('Document', backref='user', lazy='dynamic')
    audit_logs = db.relationship('AuditLog', backref='user', lazy='dynamic')

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    @classmethod
    def load(cls, data, **kwargs):
        return super().load(data=data, schema=schemas)

    @classmethod
    def dump(cls, data, **kwargs):
        return super().dump(data=data, schema=schemas)

    @classmethod
    def validate(cls, data, **kwargs):
        return super().validate(data=data, schema=schemas)
