from marshmallow import pre_load, ValidationError, RAISE
from app.utilities import CoreModel
from app import db, marsh


class NoteSchema(marsh.Schema):
    required_fields = []

    class Meta:
        fields = ()
        load_only = ()  # fields to accept ONLY when loading
        dump_only = ('id')  # fields to accept ONLY when dumping
        unknown = RAISE


schemas = NoteSchema(), NoteSchema(many=True)


class Note(CoreModel):
    __tablename__ = 'notes'
    text = db.Column(db.String(499))
    company_id = db.Column(db.Integer, db.ForeignKey('companies.id'), index=True, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), index=True, nullable=False)

    # Optional foreign keys
    lease_id = db.Column(db.Integer, db.ForeignKey('leases.id'))
    property_id = db.Column(db.Integer, db.ForeignKey('properties.id'))
    unit_id = db.Column(db.Integer, db.ForeignKey('units.id'))
    work_request_id = db.Column(db.Integer, db.ForeignKey('work_requests.id'))

    @classmethod
    def load(cls, data, **kwargs):
        return super().load(data=data, schema=schemas)

    @classmethod
    def dump(cls, data, **kwargs):
        return super().dump(data=data, schema=schemas)

    @classmethod
    def validate(cls, data, **kwargs):
        return super().validate(data=data, schema=schemas)
