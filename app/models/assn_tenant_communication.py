from app import db

communications_with_tenants = db.Table(
    'communications_with_tenants',
    db.Column('tenant_id', db.Integer, db.ForeignKey('tenants.id'), index=True, nullable=False),
    db.Column('communication_id', db.Integer, db.ForeignKey('communications.id'), index=True, nullable=False)
)
