from marshmallow import validates, pre_load, ValidationError
from app import app, db, marsh
from app.utilities import CoreModel


class PropertySchema(marsh.Schema):
    class Meta:
        fields = ('id', 'name', 'street_addr_line_1', 'street_addr_line_2', 'street_addr_line_3', 'city', 'state', 'zip',
                  'country', 'property_type', 'company_id')
        dump_only = ('id', 'company_id')
        unknown = 'raise'


schemas = PropertySchema(), PropertySchema(many=True)


class Property(CoreModel):
    __tablename__ = 'properties'
    name = db.Column(db.String(99), nullable=False)
    street_addr_line_1 = db.Column(db.String(149))
    street_addr_line_2 = db.Column(db.String(149))
    street_addr_line_3 = db.Column(db.String(149))
    city = db.Column(db.String(149))
    state = db.Column(db.String(9))
    zip = db.Column(db.String(9))
    country = db.Column(db.String(99))
    property_type = db.Column(db.String(29))
    company_id = db.Column(db.ForeignKey('companies.id'), index=True, nullable=False)

    # Relationships
    documents = db.relationship('Document', backref='property', lazy='dynamic')
    notes = db.relationship('Note', backref='property', lazy='dynamic')
    units = db.relationship('Unit', backref='property', lazy='dynamic')

    @classmethod
    def load(cls, data, **kwargs):
        return super().load(data=data, schema=schemas)

    @classmethod
    def dump(cls, data, **kwargs):
        return super().dump(data=data, schema=schemas)

    @classmethod
    def validate(cls, data, **kwargs):
        return super().validate(data=data, schema=schemas)
