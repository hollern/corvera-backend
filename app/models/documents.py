from marshmallow import pre_load, ValidationError, RAISE
from app.utilities import CoreModel
from app import db, marsh


class DocumentSchema(marsh.Schema):
    required_fields = []

    class Meta:
        fields = ()
        load_only = ()  # fields to accept ONLY when loading
        dump_only = ('id')  # fields to accept ONLY when dumping
        unknown = RAISE


schemas = DocumentSchema(), DocumentSchema(many=True)


class Document(CoreModel):
    __tablename__ = 'documents'
    url = db.Column(db.String(299))  # AWS S3 URL to document link
    company_id = db.Column(db.Integer, db.ForeignKey('companies.id'), index=True, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), index=True, nullable=False)

    # Optional foreign keys
    lease_id = db.Column(db.Integer, db.ForeignKey('leases.id'), index=True)
    property_id = db.Column(db.Integer, db.ForeignKey('properties.id'), index=True)
    unit_id = db.Column(db.Integer, db.ForeignKey('units.id'), index=True)
    work_request_id = db.Column(db.Integer, db.ForeignKey('work_requests.id'), index=True)

    @classmethod
    def load(cls, data, **kwargs):
        return super().load(data=data, schema=schemas)

    @classmethod
    def dump(cls, data, **kwargs):
        return super().dump(data=data, schema=schemas)

    @classmethod
    def validate(cls, data, **kwargs):
        return super().validate(data=data, schema=schemas)
