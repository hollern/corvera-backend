from app import db

tenants_on_leases = db.Table(
    'tenants_on_leases',
    db.Column('tenant_id', db.Integer, db.ForeignKey('tenants.id'), index=True, nullable=False),
    db.Column('lease_id', db.Integer, db.ForeignKey('leases.id'), index=True, nullable=False)
)
