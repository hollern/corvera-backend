from app.utilities import CoreModel
from app.models import tenants_on_leases
from marshmallow import pre_load, ValidationError
from app import db, marsh


class LeaseSchema(marsh.Schema):
    class Meta:
        fields = ('id', 'rent_cycle', 'first_due_date', 'amount_due', 'memo', 'security_deposit_amount',
                  'is_security_deposit_paid', 'is_lease_signed', 'unit_id')
        dump_only = ('id')
        unknown = 'raise'


schemas = LeaseSchema(), LeaseSchema(many=True)


class Lease(CoreModel):
    __tablename__ = 'leases'
    rent_cycle = db.Column(db.String(19))
    first_due_date = db.Column(db.Date)
    amount_due = db.Column(db.Float)
    memo = db.Column(db.String(299))
    security_deposit_amount = db.Column(db.Float)
    is_security_deposit_paid = db.Column(db.Boolean, default=False)
    is_lease_signed = db.Column(db.Boolean, default=False)
    unit_id = db.Column(db.Integer, db.ForeignKey('units.id'), index=True, nullable=False)
    company_id = db.Column(db.Integer, db.ForeignKey('companies.id'), index=True, nullable=False)

    # Relationships
    documents = db.relationship('Document', backref='lease', lazy='dynamic')
    notes = db.relationship('Note', backref='lease', lazy='dynamic')
    tenants = db.relationship('Tenant', secondary=tenants_on_leases,
                              backref=db.backref('lease', lazy='dynamic'), lazy='dynamic')

    @classmethod
    def load(cls, data, **kwargs):
        return super().load(data=data, schema=schemas)

    @classmethod
    def dump(cls, data, **kwargs):
        return super().dump(data=data, schema=schemas)

    @classmethod
    def validate(cls, data, **kwargs):
        return super().validate(data=data, schema=schemas)
