from marshmallow import validates, pre_load, ValidationError, RAISE
from app import app, db, marsh
from app.utilities import CoreModel


class PaymentMadeSchema(marsh.Schema):
    class Meta:
        fields = ()
        dump_only = ('id')


schemas = PaymentMadeSchema(), PaymentMadeSchema(many=True)


class PaymentMade(CoreModel):
    __tablename__ = 'payments_made'
    paid_date = db.Column(db.Date)
    amount = db.Column(db.Float)
    company_id = db.Column(db.ForeignKey('companies.id'), index=True, nullable=False)

    @classmethod
    def load(cls, data, **kwargs):
        return super().load(data=data, schema=schemas)

    @classmethod
    def dump(cls, data, **kwargs):
        return super().dump(data=data, schema=schemas)

    @classmethod
    def validate(cls, data, **kwargs):
        return super().validate(data=data, schema=schemas)
